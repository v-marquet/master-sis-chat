import java.io.*;
import java.security.*;
import java.security.spec.*;
import java.math.BigInteger;
import javax.crypto.Cipher;
import javax.crypto.spec.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.util.Random;
import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;


// codé à l'arrache ! archi-moche !!!


public class MyDES {
	private static BASE64Encoder enc = new BASE64Encoder();
    private static BASE64Decoder dec = new BASE64Decoder();

	private static final byte[] theDESKey = 
        new byte[] { (byte)0xCA, (byte)0xCA, (byte)0xCA, (byte)0xCA, (byte)0xCA, (byte)0xCA, (byte)0xCA, (byte)0xCA };
    private static SecretKeySpec key;
    private static Cipher cipher;

	MyDES() {
		
	}

	// Exception management to rewrite !!!
	public static byte[] encrypt(byte[] input) {
		try {
			Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
			key = new SecretKeySpec(theDESKey, "DES");
	  		cipher = Cipher.getInstance("DES/ECB/NoPadding", "BC");
	  	} catch(Exception e) {
	  		e.printStackTrace();
	  	}

		try {
	  		cipher.init(Cipher.ENCRYPT_MODE, key);
	  		byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
	  		int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
	  		ctLength += cipher.doFinal(cipherText, ctLength);
	  		return cipherText;
	  	} catch(Exception e) {
	  		e.printStackTrace();
	  	}
	  	return null;
	}

	public static String encryptStringTo64(String s) {
		byte[] clair = s.getBytes();
		byte[] chiffre = encrypt(addPadding(clair));
		return enc.encode(chiffre);
	}

	// Exception management to rewrite !!!
	public static byte[] decrypt(byte[] input) {
		try {
			Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
			key = new SecretKeySpec(theDESKey, "DES");
	  		cipher = Cipher.getInstance("DES/ECB/NoPadding", "BC");
	  	} catch(Exception e) {
	  		e.printStackTrace();
	  	}

		try {
	  		cipher.init(Cipher.DECRYPT_MODE, key);
	  		byte[] plainText = new byte[cipher.getOutputSize(input.length)];
	  		int ctLength = cipher.update(input, 0, input.length, plainText, 0);
	  		ctLength += cipher.doFinal(plainText, ctLength);
	  		return plainText;
  		} catch(Exception e) {
	  		e.printStackTrace();
	  	}
	  	return null;
	}

	public static String decrypt64ToString(String s) {
		try {
			byte[] chiffre = dec.decodeBuffer(s);
			byte[] clair = decrypt(chiffre);
			return clair.toString();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static byte[] addPadding(byte[] input) {
		int pad_to_add = 8 - (input.length % 8);
		if (pad_to_add == 0)
			return input;
		byte[] input_padded = new byte[input.length + pad_to_add];
		// arraycopy(Object src, int srcPos, Object dest, int destPos, int length)
		System.arraycopy(input, 0, input_padded, 0, input.length);
		for (int i=0; i<pad_to_add; i++) {
			input_padded[input.length+i] = 0x20;
		}
		return input_padded;
	}
}

