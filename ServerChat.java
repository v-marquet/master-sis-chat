import java.io.*;
import java.net.*;

public class ServerChat {
    private static final int port = 1111;

    public static void main(String[] args) {
        try {
            ServerSocket serversocket = new ServerSocket(port);
            System.out.println("Server listening on port \u001B[1m" + Integer.toString(port) + "\u001B[0m");

            boolean quit = false;
            while(!quit) {
                Socket socket = serversocket.accept();
                System.out.println("A new client is connected.");
                ServiceChat serviceChat = new ServiceChat(socket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
