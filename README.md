Compilation
-----------
```
$ javac -cp ".;./jars/*" ServerChat.java
```

(mettre dans `jars/` la lib `BouncyCastle`)


Utilisation
-----------
Serveur:
```
$ java -cp ".;./jars/*" ServerChat
```

Client:
```
$ nc localhost 1111
```


Commandes implémentées
----------------------
* /help
* /list (liste des users connectés)
* /msg user-foo message (message direct)
* /quit
