import java.io.*;
import java.net.*;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Arrays; 
import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;
import java.security.*;
import java.security.spec.*;
import java.math.BigInteger;
import javax.crypto.Cipher;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.util.Random;
import java.util.Date;



public class ServiceChat extends Thread {
    //private boolean encryptedMode = true;

    private static final int NB_CLIENTS_MAX = 3;
    private static ArrayList<User> users = new ArrayList<User>();
    private User user;

    private Socket socket;
    private BufferedReader in;

    private BASE64Encoder enc = new BASE64Encoder();
    private BASE64Decoder dec = new BASE64Decoder();

    //MyDES des = new MyDES();


    public ServiceChat(Socket socket) {
        /*System.out.println("================= TEST DES ====================");
        byte[] test = new byte[] { 0x01, 0x02, 0x03, 0x04, 0x01, 0x02, 0x03, 0x04,0x01, 0x02, 0x03, 0x04,0x01, 0x02, 0x03 };
        etst = MyDES.addPadding(test);
        System.out.println("clair = " + enc.encode(test));

        byte[] result;

        result = MyDES.encrypt(test);
        System.out.println("chffre = " + enc.encode(result));

        result = MyDES.decrypt(result);
        System.out.println("clair = " + enc.encode(result));
        System.out.println("================= END TEST DES ====================");*/


        this.socket = socket;
        if (init())
            this.start();
        else {
            try {
                socket.close();
            } catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    // init the connection  to the client (PrintStream stuff + authentication)
    private boolean init() {
        // if max number of clients is reached, we abort
        if (getNumberOfClients() == NB_CLIENTS_MAX) {
            try {
                PrintStream ps = new PrintStream(socket.getOutputStream());
                ps.println(colorRed("Max number of connections reached, please try again later."));
            } catch(IOException e) {}
            systemMessageAndLog("Client tried to connect but max number of connections reached.");
            return false;
        }

        // we create a new PrintStream to send messages to the client
        PrintStream output;
        try {
            output = new PrintStream(socket.getOutputStream());
        } catch (IOException e) {
            System.out.println("Error: init(): can't get socket output stream.");
            return false;
        }

        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            System.out.println("Error: init(): can't get socket input stream.");
            closeConnection();
            return false;
        }

        //welcomeMessage(output);

        // authentication step
        user = authenticateUser(output);
        if (user == null)
            return false;

        // is user is a newly created one, we add him to the ArrayList of users
        if (!user.isRegistered())
            addUser(user);

        return connectUser(user, output);
    }

    private void welcomeMessage(PrintStream output) {
        String message = "Welcome! There are " + getNumberOfClients() + " users connected";
        // if there is at least one client already connected, we list them
        if (getNumberOfClients() >= 1)
            message += ": " + usersList();
        message += ".";
        output.println(message);
    }

    private User authenticateUser(PrintStream output) {
        boolean userAuthenticated = false;

        while(!userAuthenticated) {
            try {
                String pseudo = promptUser(output, "Choose a pseudo:");
                boolean error = false;

                // check if this pseudo is already used
                for (User u : users) {
                    if (u.getPseudo().equals(pseudo)) {
                        // check if the user with this pseudo is already connected
                        // if yes, we prevent this user from connecting on two sessions
                        if (u.isConnected()) {
                            output.println(colorRed("Pseudo already taken / you are already connected."));
                            error = true;
                        }
                        // if not, we do a challenge/response to check his identity
                        else {



                            PublicKey pubkey = u.getPubkey();

                            // Get Cipher able to apply RSA_NOPAD
                            // (must use "Bouncy Castle" crypto provider)
                            Security.addProvider(new BouncyCastleProvider());

                            ///// generate challenge
                            int RSA_INPUT_SIZE  = 127;
                            int RSA_OUTPUT_SIZE = 128;
                            // generate random bytes
                            SecureRandom sr = new SecureRandom();
                            byte[] challenge = new byte[RSA_INPUT_SIZE];
                            sr.nextBytes(challenge);
                            System.out.println("[DEBUG] challenge = " + Utils.bytesToHex(challenge));

                            // encrypt it with public key
                            Cipher cRSA_NO_PAD = Cipher.getInstance( "RSA/NONE/NoPadding", "BC" );
                            cRSA_NO_PAD.init( Cipher.ENCRYPT_MODE, pubkey );
                            System.out.println("CIPHER SIZE = " + cRSA_NO_PAD.getBlockSize());
                            byte[] ciphered = new byte[RSA_OUTPUT_SIZE];
                            // DOC: doFinal(byte[] input, int inputOffset, int inputLen, byte[] output, int outputOffset)
                            cRSA_NO_PAD.doFinal(challenge, 0, RSA_INPUT_SIZE, ciphered, 0);

                            // convert to Base64 for transport
                            
                            String ciphered_b64 = enc.encode(ciphered);
                            ciphered_b64 = ciphered_b64.replaceAll("\r\n", "");
                            ciphered_b64 = ciphered_b64.replaceAll("\n", "");

                            // get response
                            String challenge_answer_b64 = promptUser(output, "Sending challenge:" + ciphered_b64);
                            String challenge_answer = Utils.bytesToHex(dec.decodeBuffer(challenge_answer_b64));

                            // check challenge answer
                            System.out.println("[DEBUG] challenge answer = " + challenge_answer);
                            if (challenge_answer.contains(Utils.bytesToHex(challenge)))
                                return u;
                            else {
                                output.println(colorRed("Wrong answer."));
                                error = true;
                            }




                        }
                        break;
                    }
                }
                if (error)
                    continue;

                // before validating, we must check that the pseudo is correct
                if (pseudo.length() < 3 && pseudo.length() > 20) {
                    output.println(colorRed("Pseudo length must be >= 3 and <= 20."));
                    continue;
                }

                if (!pseudo.matches("[a-z]+")) {
                    output.println(colorRed("Pseudo must contain only lowercase ASCII letters."));
                    continue;
                }


                // we ask the user to send the modulus
                String modulus_b64 = promptUser(output, "Send RSA modulus:");
                byte[] modulus = dec.decodeBuffer(modulus_b64);
                System.out.println("[DEBUG] modulus = " + Utils.bytesToHex(modulus));

                // we ask the user to send the exponent
                String exponent_b64 = promptUser(output, "Send RSA exponent:");
                byte[] exponent = dec.decodeBuffer(exponent_b64);
                System.out.println("[DEBUG] exponent = " + Utils.bytesToHex(exponent));

                // we create public key from modulus and exponent
                RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(Utils.bytesToHex(modulus),16), 
                                                            new BigInteger(Utils.bytesToHex(exponent),16));
                KeyFactory factory = KeyFactory.getInstance("RSA");
                PublicKey pubkey = factory.generatePublic(spec);

                // Get Cipher able to apply RSA_NOPAD
                // (must use "Bouncy Castle" crypto provider)
                Security.addProvider(new BouncyCastleProvider());

                System.out.println("MOD = " + (new BigInteger(modulus)).toString());
                System.out.println("EXP = " + (new BigInteger(exponent)).toString());

                ///// generate challenge
                int RSA_INPUT_SIZE  = 127;
                int RSA_OUTPUT_SIZE = 128;
                // generate random bytes
                SecureRandom sr = new SecureRandom();
                byte[] challenge = new byte[RSA_INPUT_SIZE];
                sr.nextBytes(challenge);
                //System.out.println("[DEBUG] challenge = " + Utils.bytesToHex(challenge));

                // encrypt it with public key
                Cipher cRSA_NO_PAD = Cipher.getInstance( "RSA/NONE/NoPadding", "BC" );
                cRSA_NO_PAD.init( Cipher.ENCRYPT_MODE, pubkey );
                System.out.println("CIPHER SIZE = " + cRSA_NO_PAD.getBlockSize());
                byte[] ciphered = new byte[RSA_OUTPUT_SIZE];
                // DOC: doFinal(byte[] input, int inputOffset, int inputLen, byte[] output, int outputOffset)
                cRSA_NO_PAD.doFinal(challenge, 0, RSA_INPUT_SIZE, ciphered, 0);

                // convert to Base64 for transport
                String ciphered_b64 = enc.encode(ciphered);
                ciphered_b64 = ciphered_b64.replaceAll("\r\n", "");
                ciphered_b64 = ciphered_b64.replaceAll("\n", "");

                // get response
                String challenge_answer_b64 = promptUser(output, "Sending challenge:" + ciphered_b64);
                String challenge_answer = Utils.bytesToHex(dec.decodeBuffer(challenge_answer_b64));

                // check challenge answer
                //System.out.println("[DEBUG] challenge answer = " + challenge_answer);
                if (!challenge_answer.contains(Utils.bytesToHex(challenge)))
                    continue;

                // if no problem, we add user in the list of registered users
                User u =  new User(pseudo, pubkey);
                System.out.println("Client choose pseudo " + u.getPseudoColored() + ".");
                return u;
            } catch (IOException e) {
                System.out.println("Client quit before authenticating");
                closeConnection();
                break;
            } catch (Exception e) {
                e.printStackTrace();
                closeConnection();
                break;
            }
        }

        return null;
    }

    private String promptUser(PrintStream output, String message) throws IOException {
        output.println(message);
        String answer = in.readLine();
        if (answer == null)
            throw new IOException("error: pseudo is null");
        return answer.trim();
    }

    public void run() {
        systemMessage("New user connected: " + user.getPseudoColored());

        String line;
        while(true) {
            try {
                line = in.readLine();
                if (line == null)
                    throw new IOException();
                else {
                    /*if (encryptedMode) {
                        System.out.println("[DEBUG] recu chiffre = " + line);
                        line = MyDES.decrypt64ToString(line);
                        System.out.println("[DEBUG] recu clair = " + line);
                    }*/

                    System.out.println("Received: " + line);

                    if (line.startsWith("/")) {  // command
                        if (line.startsWith("/help")) {
                            postMessage(line);
                            systemMessage(helpMessage());
                        }
                        else if (line.startsWith("/list")) {
                            postMessage(line);
                            systemMessage(usersListMessage());
                        }
                        else if (line.startsWith("/quit")) {
                            postMessage(line);
                            throw new IOException();
                        }
                        else if (line.startsWith("/msg")) {
                            String[] s = line.split(" ");
                            if (s.length < 3)
                                continue;
                            String pseudo = s[1].trim();
                            String message = String.join(" ", Arrays.copyOfRange(s, 2, s.length));
                            privateMessage(pseudo, message);
                        }
                    }
                    else  // regular message
                        postMessage(line);
                }
            } catch (IOException e) {
                systemMessageAndLog(user.getPseudoColored() + " has quit");
                closeConnection();
                break;
            }
        }
    }

    private void postMessage(String message) {
        broadcast(user.getColor() + user.getPseudo() + "> " + message + "\u001B[0m");
    }

    private void privateMessage(String pseudo, String message) {
        for (User u : users) {
            if (u.getPseudo().equals(pseudo)) {
                u.sendMessage("[PRIVATE]" + user.getColor() + user.getPseudo() + "> " + message + "\u001B[0m");
                return;
            }
        }
    }

    // create a system message and log the same message
    private void systemMessageAndLog(String message) {
        systemMessage(message);
        System.out.println(message);
    }

    // create a system message (special formatting) and broadcast it
    private void systemMessage(String message) {
        broadcast("\u001B[2m=== " + message + "\u001B[0m");
    }

    // send string to all clients
    private void broadcast(String string) {
        for (User u : users) {
            if (u.isConnected()) {
                /*if (encryptedMode) {
                    System.out.println("[DEBUG] send clair = " + string);
                    String chiffre = MyDES.encryptStringTo64(string);
                    System.out.println("[DEBUG] send chiffre = " + chiffre);
                    u.sendMessage(chiffre);
                }
                else*/
                    u.sendMessage(string);
            }
        }
    }

    // returns a help message with the list of commands
    private String helpMessage() {
        String message = "Available commands:";
        message += " /list: lister les users connectés,";
        message += " /quit: se déconnecter,";
        message += " /msg user-foo message: message direct";
        return message;
    }

    // returns a message with the list of users
    private String usersListMessage() {
        return "Users connected: " + usersList();
    }

    // returns a string with the pseudos of users, comma separated
    private String usersList() {
        String list = "";
        int j = 0;
        for (User u : users) {
            if (u.isConnected()) {
                list += u.getPseudoColored();
                if (++j < getNumberOfClients())
                    list += ", ";
            }
        }
        return list;
    }

    // returns the number of clients connected
    private int getNumberOfClients() {
        int numberOfClients = 0;
        for (User u : users) {
            if (u.isConnected())
                numberOfClients++;
        }
        return numberOfClients;
    }

    // add a user to the list of registered users
    private void addUser(User u) {
        u.register();
        users.add(u);
    }

    // get a user based on pseudo
    private User getUser(String pseudo) {
        for (User u : users) {
            if (u.getPseudo().equals(pseudo))
                return u;
        }
        return null;
    }

    // return true if user successfully connected, false otherwise
    private synchronized boolean connectUser(User u, PrintStream output) {
        if (getNumberOfClients() == NB_CLIENTS_MAX)
            return false;
        u.connect(output);
        return true;
    }

    private void closeConnection() {
        // first we close socket
        try {
            socket.close();
        } catch(IOException e) {
            e.printStackTrace();
        }

        // then we properly deal with our User instance if it exist
        if (user != null)
            user.deconnect();
    }


    private String color(String s, String color) {
        return color + s + "\u001B[0m";
    }

    private String colorRed(String s) {
        return color(s, "\u001B[31m");
    }
}
