import java.io.*;
import java.net.*;
import java.util.concurrent.ThreadLocalRandom;
import java.security.*;


public class User {
    private String pseudo;
    private PublicKey pubkey;
    private PrintStream output;
    private boolean isConnected;
    private boolean isRegistered;
    private String color;

    public User(String pseudo, PublicKey pubkey) {
        this.pseudo = pseudo;
        this.isRegistered = false;
        // we choose a color randomly
        int random = ThreadLocalRandom.current().nextInt(2, 6 + 1); // entre 2 et 6 inclus
        this.color = "\u001B[3" + random + "m";
        // "\u001B[32m" = green
        // "\u001B[33m" = yellow
        // "\u001B[34m" = blue
        // "\u001B[35m" = magenta
        // "\u001B[36m" = cyan

        this.pubkey = pubkey;
    }

    // the getters
    public String getPseudo() {
        return this.pseudo;
    }

    public PublicKey getPubkey() {
        return this.pubkey;
    }

    public String getPseudoColored() {
        return this.color + this.pseudo + "\u001B[0m";
    }

    public boolean isConnected() {
        return this.isConnected;
    }

    public boolean isRegistered() {
        return this.isRegistered;
    }

    public String getColor() {
        return this.color;
    }

    // the setters
    public void connect(PrintStream output) {
        this.output = output;
        this.isConnected = true;
    }

    public void deconnect() {
        this.output = null;
        this.isConnected = false;
    }

    public void register() {
        this.isRegistered = true;
    }

    // other
    public void sendMessage(String s) {
        output.println(s);
    }
}
